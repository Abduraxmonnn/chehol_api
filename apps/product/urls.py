from django.urls import path

from . import views


urlpatterns = [
    path('chehol/category/create/', views.CheholCategoryCreateAPIView.as_view(), name='chehol-category-create'),
    path('chehol/categories/list/', views.CheholCategoryListAPIView.as_view(),
         name='chehol-categories-list'),
    path('chehol/category/detail/<int:pk>/', views.CheholCategoryRetrieveAPIVIew.as_view(),
         name='chehol-category-detail'),
    path('chehol/category/update/<int:pk>/', views.CheholCategoryUpdateViewSet.as_view({'put': 'update'}),
         name='chehol-update-detail'),
    path('chehol/category/destroy/<int:pk>/', views.CheholCategoryDestroyAPIView.as_view(),
         name='chehol-category-destroy'),
    path('chehol/product/create/', views.CheholCreateUpdateViewSet.as_view({'post': 'create'}),
         name='chehol-product-create'),
    path('chehol/products/list/', views.CheholListViewSet.as_view({'get': 'list'}), name='chehol-products-list'),
    path('chehol/product/detail/<int:pk>/', views.CheholRetrieveAPIView.as_view(),
         name='chehol-product-detail'),
    path('chehol/product/update/<int:pk>/', views.CheholCreateUpdateViewSet.as_view({'put': 'update'}),
         name='chehol-product-update'),
    path('chehol/products/destroy/<int:pk>/', views.CheholDestroyViewSet.as_view({'delete': 'destroy'}),
         name='chehol-product-destroy'),
]