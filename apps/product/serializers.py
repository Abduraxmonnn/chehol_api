# Rest-Framework
from rest_framework import serializers, status
# Project
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .models import CheholCategory, Chehol


class CheholCategorySerializer(serializers.ModelSerializer):

    # def post(self, validated_data):
    #     new_category = CheholCategory.objects.create(**validated_data)
    #     new_category.save(material=validated_data['material'])
    #     return new_category

    def post(self, validated_data):
        new_category = super().create(validated_data=validated_data)
        new_category.save()
        return new_category

    def get(self, request):
        product = request.product
        serializer = CheholSerializer(instance=product, context={'request': request})
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    class Meta:
        model = CheholCategory
        fields = '__all__'


class CheholSerializer(serializers.ModelSerializer):

    def post(self, request, validated_data):
        new_product = Chehol.objects.create(**validated_data)
        new_product.save(foo=validated_data['car_name', 'car_model', 'chehol', 'pattern', 'color',
                                            'price', 'image', 'phone_number', 'instagram', 'telegram'])
        return new_product

    def get(self, request):
        product = request.product
        serializer = CheholSerializer(instance=product, context={'request': request})
        return Response(data=serializer.data, status=status.HTTP_200_OK)

    class Meta:
        model = Chehol
        fields = '__all__'
