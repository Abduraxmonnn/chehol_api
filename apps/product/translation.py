from modeltranslation.translator import register, TranslationOptions
from .models import CheholCategory, Chehol


@register(CheholCategory)
class CheholCategoryTranslationOptions(TranslationOptions):
    fields = ('material', )


@register(Chehol)
class CheholTranslationOptions(TranslationOptions):
    fields = ('car_name', 'car_model', 'chehol', 'pattern', 'color')
