# Django
from django_filters.rest_framework import DjangoFilterBackend
# Rest-Framework
from rest_framework import status
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import CreateAPIView, RetrieveAPIView, DestroyAPIView, ListAPIView
# Project
from .serializers import CheholCategorySerializer, CheholSerializer
from .models import CheholCategory, Chehol


# Chehol Category ======================================================================================================
class CheholCategoryCreateAPIView(CreateAPIView):
    serializer_class = CheholCategorySerializer
    queryset = CheholCategory.objects.all()

    # permission_classes = [IsAuthenticated]

    def post(self, request, *args, **kwargs):
        chehol_category_data = request.data

        new_patient = CheholCategory.objects.create(material=chehol_category_data['material'])
        new_patient.save()
        serializer = CheholCategorySerializer(new_patient)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


class CheholCategoryListAPIView(ListAPIView):
    serializer_class = CheholCategorySerializer
    queryset = CheholCategory.objects.all()
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['id', 'material']
    ordering_fields = ['id']


class CheholCategoryRetrieveAPIVIew(RetrieveAPIView):
    serializer_class = CheholCategorySerializer
    queryset = CheholCategory.objects.all()
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['id', 'material']
    ordering_fields = ['id']

    # permission_classes = [IsAuthenticated]\


class CheholCategoryUpdateViewSet(ModelViewSet):
    serializer_class = CheholCategorySerializer
    queryset = CheholCategory.objects.filter().order_by('id')
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ['id', 'material']
    ordering_fields = ['id']

    def get_queryset(self):
        queryset = super(CheholCategoryUpdateViewSet, self).get_queryset()
        return queryset


class CheholCategoryDestroyAPIView(DestroyAPIView):
    serializer_class = CheholCategorySerializer
    queryset = CheholCategory.objects.all()

    # permission_classes = [IsAuthenticated]


# Chehol ===============================================================================================================
class CheholCreateUpdateViewSet(ModelViewSet):
    serializer_class = CheholSerializer
    queryset = Chehol.objects.all()

    # permission_classes = [IsAuthenticatedOrReadOnly]

    def get_serializer_context(self):
        return {'request': self.request}

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset()
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer):
        serializer.save()


class CheholListViewSet(ModelViewSet):
    serializer_class = CheholSerializer
    queryset = Chehol.objects.all()
    filter_backends = [DjangoFilterBackend, SearchFilter, OrderingFilter]
    filter_fields = ['car_name', 'chehol', 'color']
    search_fields = ['car_name', 'car_model', 'chehol', 'color']
    ordering_fields = ['car_name', 'color', 'price']

    # permission_classes = [IsAuthenticatedOrReadOnly]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        chehol = self.paginate_queryset(queryset)
        if chehol is not None:
            serializer = self.get_serializer(chehol, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class CheholRetrieveAPIView(RetrieveAPIView):
    serializer_class = CheholSerializer
    queryset = Chehol.objects.all()
    filter_backends = [DjangoFilterBackend]
    filter_fields = ['car_name', 'car_model', 'chehol', 'color']

    # permission_classes = [IsAuthenticatedOrReadOnly]

    def retrieve(self, request, *args, **kwargs):
        self.serializer_class = CheholSerializer
        return super(CheholRetrieveAPIView, self).retrieve(request, *args, **kwargs)


class CheholDestroyViewSet(ModelViewSet):
    serializer_class = CheholSerializer
    queryset = Chehol.objects.all()

    # permission_classes = [IsAuthenticated]

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_destroy(self, instance):
        instance.delete()
