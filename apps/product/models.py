from django.db import models


class CheholCategory(models.Model):
    material = models.CharField(max_length=50)

    class Meta:
        verbose_name = "Chehollat turi"
        verbose_name_plural = "chehollat turi"

    def __str__(self):
        return f'{self.material}'


class Chehol(models.Model):
    car_name = models.CharField(max_length=50, verbose_name="Avtomabil nomi")
    car_model = models.CharField(max_length=50, blank=False, verbose_name="Avtomabil modelli")
    chehol = models.ForeignKey(CheholCategory, on_delete=models.CASCADE, verbose_name="Chehol turi")
    pattern = models.BooleanField(default=False, blank=False, verbose_name="Cheholning naqshi")
    color = models.CharField(max_length=10, verbose_name="Cheholning ranggi")
    price = models.CharField(max_length=50, verbose_name="Chehol narxi")
    image = models.ImageField(upload_to='product/%Y/%m/%d', blank=False, verbose_name="Picture")
    phone_number = models.CharField(max_length=20, blank=False)
    instagram = models.CharField(max_length=30, blank=False, verbose_name='Instagram link')
    telegram = models.CharField(max_length=30, blank=True, verbose_name='Telegram link')

    class Meta:
        verbose_name = "Chehollar"
        verbose_name_plural = "chehollar"

    def __str__(self):
        return f'{self.car_name}, {self.car_model}, {self.chehol}, ' \
               f'{self.pattern}, {self.color}, {self.price}. {self.image}, {self.instagram}, {self.telegram}'
